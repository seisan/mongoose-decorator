'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.HOOKS = undefined;
exports.model = model;
exports.schema = schema;
exports.method = method;
exports.pre = pre;
exports.post = post;
exports.statics = statics;
exports.virtual = virtual;
exports.plugin = plugin;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HOOKS = exports.HOOKS = ['init', 'validate', 'save', 'remove', 'find', 'update'];

function model(modelName) {
    if (modelName instanceof Function) {
        var name = modelName.name;
        var _schema2 = _schema(modelName);

        return _mongoose2.default.model(name, _schema2);
    } else if ('string' !== typeof modelName) {
        throw Error('Model must specify a name');
    } else {
        return function (Clazz) {
            var schema = _schema(Clazz);
            return _mongoose2.default.model(modelName, schema);
        };
    }
}

function schema(clazz, method, descriptor) {
    clazz['$$schema'] = clazz[method]();
};

function method(clazz, method, descriptor) {
    (clazz['$$methods'] = clazz['$$methods'] || []).push(clazz[method]);
    clazz['$$method_' + method] = method;
}

function pre() {
    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
    }

    return _hook(args, 'pre');
}

function post() {
    for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
    }

    return _hook(args);
}

function statics(clazz, method, descriptor) {
    (clazz['$$statics'] = clazz['$$statics'] || []).push(clazz[method]);
}

function virtual(path) {
    var action = arguments.length <= 1 || arguments[1] === undefined ? 'get' : arguments[1];

    if ('string' !== typeof path) {
        throw new Error('A virtual must have a path');
    }

    return function (clazz, method, descriptor) {
        if (!clazz['$$virtuals']) {
            clazz['$$virtuals'] = {
                get: [],
                set: []
            };
        }

        clazz['$$virtuals'][action].push({ path: path, method: clazz[method] });
    };
}

function plugin(fn, opts) {
    if (!fn instanceof Function) {
        throw new Error('Plugin must be a function');
    }
    return function (clazz) {
        (clazz.prototype['$$plugins'] = clazz.prototype['$$plugins'] || []).push({ fn: fn, options: opts });
    };
}

function _hook(args) {
    var action = arguments.length <= 1 || arguments[1] === undefined ? 'post' : arguments[1];

    var hook = args.length === 3 ? args[1] : args[0];

    if (!HOOKS.find(function (h) {
        return hook === h;
    })) {
        throw new Error(hook + ' is not a supported hook. Supported hooks are ' + HOOKS);
    }

    if (args.length === 3) {
        if (!args[0]['$$hooks']) {
            args[0]['$$hooks'] = {
                pre: [],
                post: []
            };
        }

        args[0]['$$hooks'][action].push({ hook: hook, method: args[0][args[1]] });
    } else {
        return function (clazz, method) {
            if (!clazz['$$hooks']) {
                clazz['$$hooks'] = {
                    pre: [],
                    post: []
                };
            }
            clazz['$$hooks'][action].push({ hook: hook, method: clazz[method] });
        };
    }
}

function _schema(Clazz) {
    var schema = Clazz.prototype['$$schema'] || {};
    var methods = Clazz.prototype['$$methods'] || [];
    var hooks = Clazz.prototype['$$hooks'] || [];
    var statics = Clazz.prototype['$$statics'] || [];
    var virtuals = Clazz.prototype['$$virtuals'] || [];
    var plugins = Clazz.prototype['$$plugins'] || [];

    var clazz = new Clazz();

    clazz.add(schema);

    // Methods
    methods.forEach(function (fn) {
        clazz.method(fn.name, fn);
    });

    // Hooks - pre/post
    (hooks['pre'] || []).forEach(function (hook) {
        clazz.pre(hook.hook, hook.method);
    });
    (hooks['post'] || []).forEach(function (hook) {
        clazz.pre(hook.hook, hook.method);
    });

    // Statics
    statics.forEach(function (fn) {
        clazz.statics[fn.name] = fn;
    });

    // Virtuals - get/set
    (virtuals['get'] || []).forEach(function (virtual) {
        clazz.virtual(virtual.path)['get'](virtual.method);
    });

    (virtuals['set'] || []).forEach(function (virtual) {
        clazz.virtual(virtual.path)['set'](virtual.method);
    });

    // Plugins
    plugins.forEach(function (plugin) {
        clazz.plugin(plugin.fn, plugin.options);
    });

    return clazz;
}